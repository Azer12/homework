package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner scanner = new Scanner(System.in);
        int n = 10;
        int[] arr = new int[n];
        for( int i = 0 ; i < n ; ++i ){
            arr[i] = scanner.nextInt();
        }
        for( int i = 0 ; i < n ; ++i ){
            System.out.println( arr[i] );
        }

    }

}
