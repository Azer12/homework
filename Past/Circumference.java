//4. write program which gets input from user as radius and calculates circle circumference. (use Math.PI)

package circumference;
import java.util.Scanner;

public class Circumference {

      public static void main(String[] args) {
     
   
     Scanner sc = new Scanner(System.in);
       System.out.print("Enter the radius: ");
      
      double radius = sc.nextDouble();
      
       double circumference= Math.PI * 2*radius;
      System.out.println( "The circumference of the circle is:"+circumference) ;
   
}
    }
    