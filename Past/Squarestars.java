//5. write the program which gets integer from user as N and prints square stars as NxN 

package squarestars;
import java.util.Scanner;

public class Squarestars {

    public static void main(String[] args) {
       
        Scanner read=new Scanner (System.in) ;
        int n=read.nextInt() ;
        
        for(int i=0;i<n ;i++) {
            for(int j=0;j<n;j++) {
                System.out.print("*") ;
            }
            System.out.println();
        }
        
    }
    
}
