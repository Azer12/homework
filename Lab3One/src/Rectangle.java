
public class Rectangle{

    private double xPos;
    private double yPos;

    public double getxPos() {
        return xPos;
    }

    public double getyPos() {
        return yPos;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public void setPos(int x, int y){
        xPos = x;
        yPos = y;
    }

    private double width ;
    private double height ;
    public Rectangle () {
        xPos = 0;
        yPos = 0;
        width = 1 ;
        height = 1 ;

    }

    public Rectangle(double width,double height) {
        xPos = 0;
        yPos = 0;
        this.width = width ;
        this.height = height ;

    }

    public Rectangle (double xPos,double yPos,double width,double height){
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width ;
        this.height = height ;


    }

    public String toString() {
        return ("Rectangle ("+getxPos()+","+getyPos()+")-("+(getxPos()+width)+","+(getyPos()+height)+")") ;

    }


    public double getArea() {
        return width*height ;

    }


    public void printShape() {
        for(int x=0 ;x<height ;x++){
            for(int y=0 ;y<width;y++){
                System.out.print("*");
            }
            System.out.println("");
        }
    }


    public static void main(String[] args){
        Rectangle rec = new Rectangle();
        System.out.println(rec);
        rec.printShape();

        rec = new Rectangle(2,2,4,6) ;
        System.out.println(rec);
        rec.printShape();
    }

}
