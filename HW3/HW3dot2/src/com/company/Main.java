package com.company;
/*
Design and implement class Dog that contains instance data
        that represents the dog’s name and age.
        Define the Dog constructor to accept and initialize instance data. Include getter
        and setter methods for the name and age. Include a method to compute and return the age of
        the dog in “human years” (seven times the dog’s age). Include a toString method that returns
        a one-line description of the dog. Create a driver class called Kennel, whose main method
        instantiates and updates several Dog objects.
*/
class Dog{
    String name;
    int age;
    public int compute(){
        return 7*age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name2) {
        name = name2;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age2) {
        age = age2;
    }

    public Dog(String name2, int age2){
        name = name2;
        age = age2;
    }

    @Override
    public String toString() {
        return "Name : " +  name + "  Age :  " + age;
    }
}
public class Main {

    public static void main(String[] args) {
	// write your code here
        Dog dog1 = new Dog("alabala", 20);
        Dog dog2 = new Dog( "alabala2", 25 );
        System.out.println(dog1);
    }
}
