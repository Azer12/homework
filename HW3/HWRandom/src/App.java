import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**

 */
public class App {
    private JButton jButton;
    private JLabel jLabel;
    private JPanel jpanel;


    public App() {
        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Random rn = new Random();
                int rand = Math.abs(rn.nextInt()%100+1);
                jLabel.setText(Integer.toString(rand));
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Number Generator");

        frame.setContentPane(new App().jpanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


}
